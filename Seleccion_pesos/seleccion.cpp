#include <iostream>
#include <vector>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <cmath>


int main(){

    cv::Mat frame = cv::imread("/home/potata/openCV_ejemplos/miko2.png"); //Suponer que estos son los pulsos cardiacos

    cv::Mat eigenvalues, eigenvector, M;
    uchar *data;
    int tmp = 0, tmp2 = 0;
    double sum = 0;

    resize(frame, frame, cv::Size(frame.cols, frame.cols), cv::INTER_LINEAR);

    data = frame.data;

    for(int i = 0; i < frame.cols + frame.rows; i++){
        M = cv::Mat_<double>(frame.rows, frame.cols) << data[i];
    }

    cv::eigen(M, eigenvalues, eigenvector);

    //0.7-trimmed mean

    //First set data ascendent
    for(int j = 0; j < frame.cols + frame.rows; j++){
        for(int k = 0; k < frame.cols + frame.rows - 1; k++){
            if ((int)data[j] > (int)data[j+1]){
                tmp = (int)data[j];
                tmp2 = (int)data[j + 1];
                data[j] = tmp2;
                data[j + 1] = tmp;
            }
        }
    }

    //Only considerer the midle data
    for(int j = 0; j < frame.cols + frame.rows; j++) {
        if(j > 0.7*(frame.cols + frame.rows) && j < 0.3 *(frame.cols + frame.rows)){
            sum = sum + (int)data[j];
        }
    }

    sum = sum/(frame.cols + frame.rows); //result



    cv::imshow("miko", frame);
    cv::waitKey(0);
    return 0;
}
