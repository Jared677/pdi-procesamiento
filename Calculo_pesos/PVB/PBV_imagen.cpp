#include <iostream>
#include <vector>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <cmath>

int main(){

    //initializing variables
    cv::Mat frame = cv::imread("/home/potata/openCV_ejemplos/miko2.png"), samples, cov , mu, inverted, bgr[3], final;
    uchar *data, *data_B, *data_G, *data_R;

    data = frame.data;

    int k = 1; //Gain (nose cuanto es**)

    //Fill matrix to get covariance
    for(int i = 0; i < frame.cols + frame.rows; i++){
        samples = (cv::Mat_<double>(frame.cols, frame.rows) << data[i]);
    }

    //Get covariance Q
    cv::calcCovarMatrix(samples, cov, mu, cv::COVAR_NORMAL | cv::COVAR_ROWS);
    cov = cov / (samples.rows - 1);

    //Get inverted covariance Q^{-1}
    cv::invert(cov, inverted);


    split(frame,bgr); //Split Q^{-1}

    //Fill data
    data_B = bgr[0].data;
    data_G = bgr[1].data;
    data_R = bgr[2].data;

    //Calc  W = k * P * Q^{-1}
    for(int i = 0; i < frame.cols + frame.rows; i++){
        data_B[i] = k * 0.33*data_B[i];
        data_G[i] = k * 0.78*data_G[i];
        data_R[i] = k * 0.53*data_R[i];
    }

    //Merge
    bgr[0].data = data_B;
    bgr[1].data = data_G;
    bgr[2].data = data_R;
    merge(bgr,1,final);

    cv::waitKey(0);
    return 0;
}
