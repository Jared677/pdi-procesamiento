
#include <iostream>
#include <vector>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <cmath>
#include <fstream>

int main(){

    cv::Mat frame = cv::imread("/home/potata/openCV_ejemplos/miko2.png"), img_x,  img_y;

    //Is not good use Scalar in this case, CHANGE in the future
    cv::Scalar x_mean = 0, y_mean = 0,
               x_gamma = 0, y_gamma = 0, alfa = 0,
               W_b = 0, W_g = 0, W_r = 0;

    std::vector<float> variables;

    cv::Mat bgr[3], Var;   //destination array

    uchar *data_By, *data_Gy, *data_Ry, //Pointers to data
          *data_Bx, *data_Gx, *data_Rx;

    cv::imshow("Imagen original", frame);


    split(frame,bgr); //split source

    //Fill data in X and Y
    data_Bx = bgr[0].data;
    data_Gx = bgr[1].data;
    data_Rx = bgr[2].data;

    data_By = bgr[0].data;
    data_Gy = bgr[1].data;
    data_Ry = bgr[2].data;

    //scale all pixels per channel to get Xs
    for(int i = 0; i < bgr[0].rows + bgr[0].cols; i++ ){
            data_Bx[i] = 0.77*data_Bx[i];
            data_Gx[i] = -0.51*data_Bx[i];
            data_Rx[i] = 0;
    }


    //Merge Xs to get standard desviation
    bgr[0].data = data_Bx;
    bgr[1].data = data_Gx;
    bgr[2].data = data_Rx;
    cv::merge(bgr, 1, img_x);


    //Get standard desviation of Xs
    cv::meanStdDev(img_x, x_mean, x_gamma);
    cv::meanStdDev(img_x, x_mean, Var);

    //scale all pixels per channel to get Ys
    for(int i = 0; i < bgr[0].rows + bgr[0].cols; i++ ){
            data_By[i] = 0.77*data_By[i];
            data_Gy[i] = 0.51*data_Gy[i];
            data_Ry[i] = -0.77*data_Ry[i];
    }

    //Merge Ys to get standard desviation
    bgr[0].data = data_By;
    bgr[1].data = data_Gy;
    bgr[2].data = data_Ry;
    cv::merge(bgr, 1, img_y);

    //Get standard desviation of Ys
    cv::meanStdDev(img_y, y_mean, y_gamma);

    //Get alfa
    alfa = x_gamma/y_gamma;

    //Get W
    W_b = 1/(sqrt(6*alfa(0)*alfa(0) - 20*alfa(0) + 20)) * (2 - alfa(0));
    W_g = 1/(sqrt(6*alfa(0)*alfa(0) - 20*alfa(0) + 20)) * (2 * alfa(0) - 4);
    W_r = 1/(sqrt(6*alfa(0)*alfa(0) - 20*alfa(0) + 20)) * alfa(0);

    //Fill vector with important variables
    variables.push_back(alfa(0));
    variables.push_back(x_gamma(0));
    variables.push_back(y_gamma(0));
    variables.push_back(W_b(0));
    variables.push_back(W_g(0));
    variables.push_back(W_r(0));


    std::cout << "ALFA: " << alfa(0) << std::endl
              << "Desviacion estandar x: " << x_gamma(0) << std::endl
              << "Desviacion estandar y: " << y_gamma(0) << std::endl
              << "Componente W_b: " << W_b(0) << std::endl
              << "Componente W_g: " << W_g(0) << std::endl
              << "Componente W_r: " << W_r(0) << std::endl;

    cv::waitKey(0);
    return 0;
}
